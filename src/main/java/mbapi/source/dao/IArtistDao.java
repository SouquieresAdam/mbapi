package mbapi.source.dao;

import java.util.List;

import mbapi.bean.Artist;

/**
 * DataAccessObject for Artist
 * 
 * @author Adam
 *
 */
public interface IArtistDao {
	/**
	 * Find an artist based on a text filter
	 * 
	 * @param filter
	 * @return
	 */
	List<Artist> findArtists(String filter);
}
