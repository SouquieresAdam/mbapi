package mbapi.application;

public class ErrorJson {

	private String errorMessage;
	private String cause;

	public ErrorJson(String apiMessage, String exceptionMessage) {

		errorMessage = apiMessage;
		setCause(exceptionMessage);

	}

	/**
	 * @return the cause
	 */
	public String getCause() {
		return cause;
	}

	/**
	 * @param cause
	 *            the cause to set
	 */
	public void setCause(String cause) {
		this.cause = cause;
	}

	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * @param errorMessage
	 *            the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
