package mbapi.cache.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.StatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.BindingSet;
import org.springframework.stereotype.Component;

import mbapi.bean.Artist;
import mbapi.bean.Tags;
import mbapi.cache.CacheDaoRegistry;

/**
 * Artist implementation of AbstractCacheDao
 * 
 * @author Adam
 *
 */
@Component
public class ArtistCacheDao extends AbstractCacheDao<Artist> {

	private String QUERY_NO_TYPES = "select ?s where { ?s <http://localhost/relation/defineas> \"undefined\" . }";

	// Register itself in the registry after initialisation
	@PostConstruct
	public void init() {
		CacheDaoRegistry.register(Artist.class, this);
	}

	/**
	 * Query returning artists name not defined as group or Personn
	 */
	public List<String> queryInterestingDatas() {

		List<String> result = new ArrayList<>();

		for (BindingSet bs : this.genericTupleQuery(QUERY_NO_TYPES)) {
			result.add(bs.getBinding("s").getValue().stringValue().split("/")[4]);
		}

		return result;
	}

	/**
	 * Store an artist in BlazeGraph : relations with tags and
	 */
	public void store(Artist toStore) {

		List<Statement> stList = new ArrayList<>();

		// Create tags relationships
		if (toStore.getTags() != null) {

			for (Tags t : toStore.getTags()) {

				URIImpl subject = new URIImpl("http://localhost/object/" + toStore.getName());
				URIImpl predicate = new URIImpl("http://localhost/relation/tagged");
				Literal object = new LiteralImpl(t.getName());
				stList.add(new StatementImpl(subject, predicate, object));
			}
		}

		// And one Type (Group/Person) relationship
		String type = toStore.getType();
		URIImpl subject = new URIImpl("http://localhost/object/" + toStore.getName());
		URIImpl predicate = new URIImpl("http://localhost/relation/defineas");
		Literal object = new LiteralImpl(type);

		stList.add(new StatementImpl(subject, predicate, object));

		// Delegating connexion management - Exception Handling
		if (stList.size() > 0) {
			genericStore(stList);
		}

	}
}
