package mbapi.source.dao.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import mbapi.bean.Artist;
import mbapi.bean.MBJsonResult;
import mbapi.cache.GraphMapper;
import mbapi.source.dao.IArtistDao;
import mbapi.utils.XUserAgentInterceptor;

/**
 * MBApi implementation of IArtistDao
 * 
 * @author Adam
 *
 */
@Component("artistFinder")
public class ArtistDao implements IArtistDao {

	@Autowired
	GraphMapper gm = null;

	/**
	 * @param string
	 *            filter un artist/band name
	 */
	@Override
	public List<Artist> findArtists(String filter) {

		RestTemplate restTemplate = new RestTemplate();
		// Changing USerAgent to respect MB WS Call policy
		restTemplate.setInterceptors(Collections.singletonList(new XUserAgentInterceptor()));

		// Call MB webservices with &fmt=json switch
		MBJsonResult result = restTemplate
				.getForObject("http://musicbrainz.org/ws/2/artist/?query=" + filter + "&fmt=json", MBJsonResult.class);

		// Calling graph mapper to transfer to cache.dao layer
		gm.store(result);

		return result.getArtists();
	}

}
