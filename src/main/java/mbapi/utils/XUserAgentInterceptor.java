package mbapi.utils;

import java.io.IOException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

/**
 * HttpRequest Interceptors which change UserAgnt on every given Request
 * 
 * @author Adam
 *
 */
public class XUserAgentInterceptor implements ClientHttpRequestInterceptor {

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
			throws IOException {

		HttpHeaders headers = request.getHeaders();
		headers.add("User-Agent", "MbApiTester/1.0 ( souquieres.adam@gmail.com )");
		return execution.execute(request, body);
	}
}