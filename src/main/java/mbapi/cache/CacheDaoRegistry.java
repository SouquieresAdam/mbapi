package mbapi.cache;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import mbapi.cache.dao.AbstractCacheDao;

/**
 * Class dedicated to register and deliver the *CacheDao associated with a class
 * to store in BlazeGraph
 * 
 * @author Adam
 *
 *         Java Style Registry Pattern
 */
@Component
public class CacheDaoRegistry {

	@SuppressWarnings("rawtypes")
	private static Map<Class<?>, AbstractCacheDao> mapStorers = new HashMap<>();

	/**
	 * Register a CacheDao instance to manage a specific clazz
	 * 
	 * @param clazz
	 * @param dao
	 */
	@SuppressWarnings("rawtypes")
	public static void register(Class<?> clazz, AbstractCacheDao dao) {
		mapStorers.put(clazz, dao);
	}

	/**
	 * Retrive a CacheDao instance to manage a specific clazz
	 * 
	 * @param clazz
	 * @param dao
	 */
	@SuppressWarnings("rawtypes")
	public AbstractCacheDao getCacheDaoFor(Class<?> clazz) {
		return mapStorers.get(clazz);
	}

}
