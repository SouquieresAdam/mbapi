package mbapi.bean;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represents the result of MusicBrainz for an Artist query
 * 
 * @author Adam
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MBJsonResult {

	/**
	 * Nombre de résultats de la requête Json
	 */
	private Long count;

	/**
	 * Liste des artistes retournés (peut être nulle)
	 */
	private List<Artist> artists;

	/**
	 * @return the artists
	 */
	public List<Artist> getArtists() {

		// prevent NPE when no artist found
		if (artists == null) {
			artists = new ArrayList<>();
		}

		return artists;
	}

	/**
	 * @param artists
	 *            the artists to set
	 */
	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}

	/**
	 * @return the count
	 */
	public Long getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the count to set
	 */
	public void setCount(Long count) {
		this.count = count;
	}
}
