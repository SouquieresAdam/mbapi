package mbapi.application;

import org.apache.log4j.Logger;
import org.openrdf.OpenRDFException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import mbapi.cache.dao.ArtistCacheDao;
import mbapi.cache.repositoy.BlazeGraphRepository;
import mbapi.source.dao.IArtistDao;

/*
 * Starting point of the application  -> public static void main(String[] args)
 * 
 * and entry point for the two services of this rest API : search and helpmb
 * 
 */
@SpringBootApplication
@ImportResource(("classpath:applicationContext.xml"))
@RestController
public class Application {

	private static Logger logger = Logger.getLogger(Application.class);

	@Autowired
	IArtistDao artistFinder;

	@Autowired
	ArtistCacheDao artistCacheData;

	@Autowired
	BlazeGraphRepository repositoryHolder;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@RequestMapping(value = "/search/{filter}")
	public Object search(@PathVariable String filter) {
		try {
			return artistFinder.findArtists(filter);
		} catch (HttpServerErrorException e) {

			// Cas de l'indisponibilité du web service MusicBrainz
			logger.error("MusicBrainz Webservice unavailable : ", e);
			return new ErrorJson("MusicBrainz Webservice unavailable, please try again later", e.getMessage());
		}

	}

	@RequestMapping(value = "/helpmb")
	public Object helpmb() throws OpenRDFException {

		return artistCacheData.queryInterestingDatas();
	}

}