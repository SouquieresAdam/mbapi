package mbapi.cache.repositoy;

import java.io.File;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.springframework.stereotype.Component;

import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

/**
 * Manage/Preovide a Repository Singleton
 * 
 * @author Adam
 *
 */
@Component
public class BlazeGraphRepository {

	private static Repository bgRep = null;

	private static Logger logger = Logger.getLogger(BlazeGraphRepository.class);

	// Properties autoloaded by Spring : applicationContext.xml
	@Resource(name = "properties")
	Properties properties;

	@PostConstruct
	public void init() {

		if (bgRep == null) {

			try {
				// create a backing file for the database
				File journal = File.createTempFile("bigdata", ".jnl");
				properties.setProperty(BigdataSail.Options.FILE, journal.getAbsolutePath());

				// instantiate a sail and a Sesame repository
				BigdataSail sail = new BigdataSail(properties);
				bgRep = new BigdataSailRepository(sail);
				bgRep.initialize();
			} catch (Exception e) {
				logger.error("Erreur lors de l'inialisation du repository BlazeGraph", e);
			}

		}
	}

	public RepositoryConnection getConnection() throws RepositoryException {
		return bgRep.getConnection();
	}

}
