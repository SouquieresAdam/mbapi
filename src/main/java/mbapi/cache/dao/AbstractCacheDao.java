package mbapi.cache.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openrdf.model.Statement;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;

import mbapi.cache.repositoy.BlazeGraphRepository;
import mbapi.utils.RepositoryConnectionCallback;

/**
 * Generic abstract CacheDao responsible for holding repositoryProvider for
 * implementations
 * 
 * @author Adam
 *
 * @param <B>
 */
public abstract class AbstractCacheDao<B> {

	private static Logger logger = Logger.getLogger(AbstractCacheDao.class);

	@Autowired
	protected BlazeGraphRepository repositoryProvider = null;

	/**
	 * Store a <B> object
	 */
	public abstract void store(B toStore);

	protected List<BindingSet> genericTupleQuery(String querySPARQL) {

		return this.genericOperation(new RepositoryConnectionCallback() {

			@Override
			public List<BindingSet> doSomethingWithConnection(RepositoryConnection c)
					throws RepositoryException, MalformedQueryException, QueryEvaluationException {

				List<BindingSet> results = new ArrayList<>();

				final TupleQuery tupleQuery = c.prepareTupleQuery(QueryLanguage.SPARQL, querySPARQL);
				TupleQueryResult result = tupleQuery.evaluate();
				try {
					while (result.hasNext()) {
						results.add(result.next());
					}
				} finally {
					result.close();
				}

				return results;
			}
		});

	}

	/**
	 * Handle for implementation the connection management and exception
	 * handling
	 * 
	 * @param sts
	 */
	protected void genericStore(final List<Statement> sts) {

		this.genericOperation(new RepositoryConnectionCallback() {

			@Override
			public List<BindingSet> doSomethingWithConnection(RepositoryConnection c) throws RepositoryException {

				c.add(sts);
				return null;
			}
		});

	}

	/**
	 * Handle the connexion management and exception handling using Callback
	 * 
	 * @param sts
	 * @return
	 */
	protected List<BindingSet> genericOperation(RepositoryConnectionCallback cb) {

		RepositoryConnection cnx = null;
		List<BindingSet> res = null;
		try {

			cnx = repositoryProvider.getConnection();
			cnx.begin();
			res = cb.doSomethingWithConnection(cnx);
			cnx.commit();

		} catch (Exception e) {

			logger.error("erreur lors de l'opération BlazeGraph", e);

		} finally {
			if (cnx != null) {
				try {
					cnx.close();
				} catch (RepositoryException e) {
					logger.error("erreur lors de la fermeture de la connexion BlazeGraph", e);

				}
			}
		}

		return res;

	}

}
