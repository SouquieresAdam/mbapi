package mbapi.utils;

import java.util.List;

import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

public interface RepositoryConnectionCallback {

	List<BindingSet> doSomethingWithConnection(RepositoryConnection c)
			throws RepositoryException, MalformedQueryException, QueryEvaluationException;

}
