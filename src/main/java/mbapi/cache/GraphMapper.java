package mbapi.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import mbapi.bean.Artist;
import mbapi.bean.MBJsonResult;

/**
 * * @author Adam
 *
 * Class dedicated to explore MBJsonResult and use registry to store every
 * single composent in BlazeGraph
 *
 */
@Component
public class GraphMapper {

	@Autowired
	CacheDaoRegistry daoRegistry = null;

	@SuppressWarnings("unchecked")
	public void store(MBJsonResult res) {

		// For now only Artist is managed
		for (Artist a : res.getArtists()) {
			daoRegistry.getCacheDaoFor(a.getClass()).store(a);
			;
		}

	}

}
