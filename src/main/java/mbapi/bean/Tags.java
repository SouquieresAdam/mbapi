package mbapi.bean;

/**
 * Represent A keyword associated to an artist
 * 
 * @author Adam
 *
 */
public class Tags {

	/**
	 * number of people who associate this tag
	 */
	private Integer count;

	/**
	 * Keyword associated
	 */
	private String name;

	/**
	 * @return the the weigth of the tag
	 */
	public Integer getCount() {
		return count;
	}

	/**
	 * @param count
	 *            the weigth of the tag to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

}
