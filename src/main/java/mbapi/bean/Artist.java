package mbapi.bean;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Represent an artist
 * 
 * @author Adam
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Artist {

	private static final String UNDEFINED = "undefined";

	/**
	 * Artist id in MusicBrainz DB
	 */
	private String id;

	/**
	 * Person or group or undefined(null)
	 */
	private String type;

	/**
	 * Search Score given by MB at the query time
	 */
	private String score;

	/**
	 * Artist name
	 */
	private String name;

	/**
	 * Artist linked tags
	 */
	private List<Tags> tags;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the type
	 */
	public String getType() {

		type = type == null ? UNDEFINED : type;

		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the score
	 */
	public String getScore() {
		return score;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(String score) {
		this.score = score;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the tags
	 */
	public List<Tags> getTags() {

		// prevent NPE when no tags
		if (tags == null) {
			tags = new ArrayList<>();
		}

		return tags;
	}

	/**
	 * @param tags
	 *            the tags to set
	 */
	public void setTags(List<Tags> tags) {
		this.tags = tags;
	}

}